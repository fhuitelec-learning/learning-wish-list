## 📚 My learning list

This repository allows me to put together and prioritize what I want to learn.

You can browse [this board](https://gitlab.com/fhuitelec-learning/learning-wish-list/boards/342739) to follow what I am currently learning and what's next.

For a more detailed view, you can go to the [issues](https://gitlab.com/fhuitelec-learning/learning-wish-list/issues).

⚠️ This is a work in progress, not everything has been put in this repository (far from it, in fact).
